<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Validator; 
use Log;

class ApiController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Api Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles api requets for the application.
    |
    */

    /**
     * Get the list of vehicles
     *
     * @param Request $request
     * @param $modelYear
     * @param $manufacturer
     * @param $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function listVehicles(Request $request, $modelYear, $manufacturer, $model) {

        $filteredResponse = array('Count' => 0, 'Results' => []);

        try{
            $route = 'modelyear'.'/'.$modelYear.'/'.'make'.'/'.$manufacturer.'/'.'model'.'/'.$model;      

            $client = new Client(['base_uri' => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/']);

            $response = $client->request('GET', $route, [
                'query' => ['format' => 'json']
            ]);
            
            $responseBody = $response->getBody(); 
            $decodedResponse = json_decode($responseBody, true);

            if($response->getStatusCode() == '200' && isset($decodedResponse['Results'])){

                //check if withRating parameter is set 
                if($request->get('withRating') == 'true'){

                    $filteredResponse['Count'] = $decodedResponse['Count'];
                    
                    foreach($decodedResponse['Results'] as $result){
                        
                        $crashRating = $this->getVehicleRating($result['VehicleId']);

                        array_push($filteredResponse['Results'], [
                            'VehicleId' => $result['VehicleId'],
                            'VehicleDescription'   => $result['VehicleDescription'],
                            'CrashRating' => $crashRating['value']
                          ]);              
                    }

                    return response()->json($filteredResponse, 200);

                } else {
                    $filteredResponse['Count'] = $decodedResponse['Count'];
                    $filteredResponse['Results'] = $decodedResponse['Results'];

                    return response()->json($filteredResponse, 200);
                }

            } else {
                return response()->json($filteredResponse, 200);    
            }
        
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json($filteredResponse, 200);  
        } 
 
    }

    /**
     * Post to get list of vehicles
     *
     * @param $vehicleId
     * @return array
     */
    public function getVehicleRating($vehicleId){
        
        $rating = array('value' => 'Not Rated');
        try{
            $route = 'VehicleId'.'/'.$vehicleId;      

            $client = new Client(['base_uri' => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/']);

            $response = $client->request('GET', $route, [
                'query' => ['format' => 'json']
            ]);
            
            $responseBody = $response->getBody(); 
            $decodedResponse = json_decode($responseBody, true);

            if($response->getStatusCode() == '200' && $decodedResponse['Count'] > 0){

                $rating['value'] = $decodedResponse['Results'][0]['OverallRating'];
                return $rating;

            } else {
                return $rating;    
            }
        
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return $rating;  
        } 
    }

    /**
     * Post to get list of vehicles
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postVehicles(Request $request) {

        $filteredResponse = array('Count' => 0, 'Results' => []);

        $validator = Validator::make($request->all(), [
            'modelYear' => 'required',
            'manufacturer' => 'required',
            'model' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($filteredResponse, 200);
        }

        $modelYear = $request->input('modelYear');
        $manufacturer = $request->input('manufacturer');
        $model = $request->input('model');

        try{
            $route = 'modelyear'.'/'.$modelYear.'/'.'make'.'/'.$manufacturer.'/'.'model'.'/'.$model;

            $client = new Client(['base_uri' => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/']);

            $response = $client->request('GET', $route, [
                'query' => ['format' => 'json']
            ]);
            
            $responseBody = $response->getBody(); 
            $decodedResponse = json_decode($responseBody, true);

            if($response->getStatusCode() == '200' && isset($decodedResponse['Results'])){
    
                $filteredResponse['Count'] = $decodedResponse['Count'];
                $filteredResponse['Results'] = $decodedResponse['Results'];

                return response()->json($filteredResponse, 200);

            } else {
                return response()->json($filteredResponse, 200);    
            }
        
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json($filteredResponse, 200);  
        } 
 
    }
    
}
