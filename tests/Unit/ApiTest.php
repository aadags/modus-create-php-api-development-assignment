<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testlistVehicles()
    {
        $response = $this->json('GET', '/vehicles/2015/Audi/A3');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Count',
                'Results' => [[
                    'VehicleDescription',
                    'VehicleId'
                ]]
            ]);
    }

    /**
     *
     * @return void
     */
    public function testlistVehiclesWithRating()
    {
        $response = $this->json('GET', '/vehicles/2015/Audi/A3?withRating=true');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Count',
                'Results' => [[
                    'VehicleDescription',
                    'VehicleId',
                    'CrashRating'
                ]],
            ]);
    }

    /**
     *
     * @return void
     */
    public function testPostVehicles()
    {
        $response = $this->json('POST', '/vehicles', ['modelYear' => '2015', 'manufacturer' => 'Audi', 'model'=> 'A3']);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Count',
                'Results' => [[
                    'VehicleDescription',
                    'VehicleId'
                ]],
            ]);
    }
    
}
